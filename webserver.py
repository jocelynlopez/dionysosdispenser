#!/usr/bin/env python3

from flask import Flask, jsonify
from flask import request
from flask import make_response
import logging
import json
import random
from pprint import pprint

# ssh -R dispenser60.serveo.net:80:localhost:5000 serveo.net

logger = logging.getLogger()
logger.setLevel(logging.INFO)
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(filename)s - %(funcName)s - %(message)s')

app = Flask(__name__)


def get_intent_from_req(req):
    """ Get intent name from dialogflow request"""
    try:
        intent_name = req['queryResult']['intent']['displayName']
    except KeyError:
        return None
    return intent_name


def create_response(response):
    """ Creates a JSON with provided response parameters """

    # convert dictionary with our response to a JSON string
    res = json.dumps(response, indent=4)

    logger.info(res)

    r = make_response(res)
    r.headers['Content-Type'] = 'application/json'

    return r

@app.route('/', methods=['GET', 'POST'])
def create_task():
    req = request.get_json(silent=True, force=True)
    fp = open("request.json", "w")
    json.dump(req, fp, indent=4)
    fp.close()
    logger.info("Incoming request: %s", req)

    intent = get_intent_from_req(req)
    logger.info('Detected intent %s', intent)

    querytext = req['queryResult']['queryText']
    print("------------> %s" % querytext)
    if intent == "request available drinks":
        response = {'fulfillmentText': "Désolé, il faut que j'aille faire caca"}
    else:
        response = {"fulfillmentText": "Non, j'ai pas envie humain"}
    if querytext == "dis bonjour aux enfants":
        response = {'fulfillmentText': "Biensûr, Bonjour, enora, aedane, swan, noelynn"}
    elif querytext in ["est ce que mael est sage", "est-ce que Maël est sage"]:
        response = {'fulfillmentText': "Ah oui, il est très très gentil, ou pas."}
    else:
        if intent == "request available drinks":
            response = {'fulfillmentText': "Pour les enfants, il y a du jus d'orange. Pour jeremy et emma, du coca rouge ! Et pour les poivraux comme Hendrick du bon Rhum !"}
        else:
            response = {'fulfillmentText': "Je suis pas ton esclave, humain !"}
    res = create_response(response)
    return res


if __name__ == '__main__':
    app.run(debug=True, port=5000, host='0.0.0.0', threaded=True)

